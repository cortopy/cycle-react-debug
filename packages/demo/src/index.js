import { div, makeDOMDriver, p } from "@cycle/react-dom";
import * as Run from "@cycle/run";
import { myNonWorkingButton } from "common";
import Stream from "xstream";
import { myWorkingButton } from "./myWorkingComponent";

function main(sources) {
  const inc = Symbol();
  const dec = Symbol();
  const action = Stream.merge(
    sources.react
      .select(dec)
      .events("click")
      .map(() => -1),
    sources.react
      .select(inc)
      .events("click")
      .map(() => 1)
  );
  const count = action.fold((acc, x) => acc + x, 0);
  return {
    react: count.map(count =>
      div([
        // this button works, as imported from file in same directory
        myWorkingButton(dec, "Decrement"),
        //  but this one from another package doesn't even though it's the same code
        myNonWorkingButton(inc, "Increment"),
        p("Counter: " + count)
      ])
    )
  };
}

Run.run(main, {
  react: makeDOMDriver(document.getElementById("app"))
});
